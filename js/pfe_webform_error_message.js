(function ($) {
  Drupal.behaviors.pfeWebformErrorMessage = {
    attach: function (context) {
      $(document).bind('clientsideValidationAddCustomRules', function(event){
        jQuery.validator.addMethod("is_empty", function(value, element, param) {
          //return true if valid, false if invalid
          return value;
          //Enter a default error message.
        }, jQuery.format('Value must a valid text'));

        var current_email = '';
        jQuery.validator.addMethod("is_equal", function(value, element, param) {
          console.log(value);
          if (!current_email) {
            current_email = value;
          }
          if (current_email == value) {
            return true;
          }
          else {
            return false;
          }
          //Enter a default error message.
        }, jQuery.format('Value must a valid text'));

      });
    }
  }
})(jQuery);
