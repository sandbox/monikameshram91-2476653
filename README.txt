Description
-----------
The Webform custom error message module adds the rule in the Webform Validation module
to add the custom error message for the various fields of the webform.
This can also be used with the Clientside validation module.

Requirements
------------
Drupal 7.x

Installation
------------
1. Copy the entire webform directory the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

3. Create a webform node at node/add/webform, in Webform Validation tab, two new rules will be enabled, Is Empty and Is Equal.
