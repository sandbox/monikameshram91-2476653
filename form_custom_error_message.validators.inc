<?php
/**
 * Implements hook_webform_validation_validators().
 * https://www.drupal.org/node/908600
 */
function form_custom_error_message_webform_validation_validators() {
  $webform_custom_validation = array();
  
  // Validator for checking empty form field.
  $webform_custom_validation['is_empty'] = array(
    'name' => "Is Empty",
    'component_types' => array ('textfield', 'select','checkbox','email'),
		'custom_error' => true,
		'custom_data' => true,
		'description' => t('Custom validation for the mandatory fields'),
  );	

  // Validator for checking if two form fields values are same.
  $webform_custom_validation['is_equal'] = array(
		'name' => "Is Equal",
		'component_types' => array ('textfield','email'),
		'custom_error' => true,
		'custom_data' => true,
		'min_components' => 2,
		'description' => t('Custom validation for the mandatory fields'),
  );	

  return $webform_custom_validation;
}

/**
 * Implements hook_webform_validation_validate().
 */
function form_custom_error_message_webform_validation_validate($validator_name, $items, $components, $rule){
  $error = array();
  if ($items) {
    switch ($validator_name) {
      // Checks for the empty form field.
      case 'is_empty' : 
        foreach ($items as $key => $value) {
          if (empty($value)) {
            $error[$key] = _webform_validation_i18n_error_message($rule);					 
          }
          elseif (is_array($value)) {
            $arr_count = array_count_values($value); 
            if ($arr_count[0] == count($value)) {
              $error[$key] = _webform_validation_i18n_error_message($rule);
            } 
          }					
        }	
      break;
      
      // Checks for the similar form fields.
      case 'is_equal' : 
        $field_values = array();
        foreach ($items as $key => $value) {
          $field_values[$key] = strtolower($value);
        } 
        $values_count = array_count_values($field_values);
        $key_count = count($values_count);
        $item_count = count($items);
        $last_key = key($items);
        if ($key_count != 1 && ($item_count != $values_count)) {						
          $error[$last_key] = _webform_validation_i18n_error_message($rule);
        }		
      break;		
    }
  }
  return $error;
}

/**
 * Implementation of hook_clientside_validation_rule_alter().
 */
function pfe_webform_error_message_clientside_validation_rule_alter (&$js_rules, $element, $context) {
  switch ($context['type']) {
    case 'webform':
      if ($context['rule']['validator'] == 'is_empty') {
        drupal_add_js(drupal_get_path('module', 'pfe_webform_error_message') . '/js/pfe_webform_error_message.js');
        $js_rules[$element['element_name']]['is_empty'] = TRUE;
        $js_rules[$element['element_name']]['messages']['is_empty'] = theme('clientside_error', array('message' => $context['message']));
      }
      if ($context['rule']['validator'] == 'is_equal') {
        drupal_add_js(drupal_get_path('module', 'pfe_webform_error_message') . '/js/pfe_webform_error_message.js');
        $js_rules[$element['element_name']]['is_equal'] = TRUE;
        $js_rules[$element['element_name']]['messages']['is_equal'] = theme('clientside_error', array('message' => $context['message']));
      }
      break;
  }
}
